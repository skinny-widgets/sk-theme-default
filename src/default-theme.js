
import { SkTheme } from "../../sk-core/src/sk-theme.js";


export class DefaultTheme extends SkTheme {

    get basePath() {
        if (! this._basePath) {
            this._basePath = (this.configEl && typeof this.configEl.hasAttribute === 'function'
                && this.configEl.hasAttribute('theme-path'))
                ? `${this.configEl.getAttribute('theme-path')}` : '/node_modules/sk-theme-default';
        }
        return this._basePath;
    }

    get styles() {
        if (! this._styles) {
            this._styles = {
                'default.css': `${this.basePath}/default.css`
            };
        }
        return this._styles;
    }
}